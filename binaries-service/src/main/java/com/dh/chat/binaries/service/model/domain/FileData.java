package com.dh.chat.binaries.service.model.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Santiago Mamani
 */
@Data
@Document(collection = "filedata")
public class FileData {

    @Id
    private String id;

    private byte[] value;

    private String fileId;

    @DBRef
    private FileImpl file;
}
