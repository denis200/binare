package com.dh.chat.binaries.service.service;

import com.dh.chat.binaries.service.model.domain.FileImpl;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class FileCreateCmd extends AbstractFileCmd {

    @Override
    protected FileImpl loadFileInstance() {
        return new FileImpl();
    }
}
