package com.dh.chat.binaries.service.service;

import com.dh.chat.binaries.service.model.domain.FileImpl;
import com.dh.chat.binaries.service.model.repository.FileRepository;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author Santiago Mamani
 */
abstract class AbstractFileCmd implements BusinessLogicCommand {

    @Setter
    private MultipartFile multipartFile;

    @Getter
    private FileImpl file;

    @Autowired
    private FileRepository repository;

    @Autowired
    private BusinessLogicCommandFactory commandFactory;

    @Override
    public void execute() {
        FileImpl instance = loadFileInstance();

        putFileInformation(instance);

        file = repository.save(instance);

        saveFileContent();
    }

    protected abstract FileImpl loadFileInstance();

    private void putFileInformation(FileImpl instance) {
        instance.setName(multipartFile.getOriginalFilename());
        instance.setSize(multipartFile.getSize());
        instance.setMimeType(multipartFile.getContentType());
    }

    private void saveFileContent() {
        try {
            FileDataCreateOrUpdateCmd command = commandFactory.createInstance(FileDataCreateOrUpdateCmd.class);
            command.setFile(file);
            command.setBytes(multipartFile.getBytes());

            command.execute();
        } catch (IOException e) {
            //  throw new FileCanNotReadException("Cannot read file");
        }
    }
}
