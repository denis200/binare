package com.dh.chat.binaries.service.model.domain;

import com.dh.chat.binaries.api.output.File;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author Santiago Mamani
 */
@Data
@Document(collection = "file")
public class FileImpl implements File {

    @Id
    private String id;

    @Field("type")
    private String mimeType;

    @Field("name")
    private String name;

    private Long size;
}
