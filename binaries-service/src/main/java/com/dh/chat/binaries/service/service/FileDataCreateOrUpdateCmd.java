package com.dh.chat.binaries.service.service;

import com.dh.chat.binaries.service.model.domain.FileData;
import com.dh.chat.binaries.service.model.domain.FileImpl;
import com.dh.chat.binaries.service.model.repository.FileDataRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class FileDataCreateOrUpdateCmd implements BusinessLogicCommand {

    @Setter
    private FileImpl file;

    @Setter
    private byte[] bytes;

    @Autowired
    private FileDataRepository repository;

    @Override
    public void execute() {
        FileData fileData = findFileData(file.getId());

        if (null == fileData) {
            fileData = new FileData();
            fileData.setFile(file);
            fileData.setFileId(file.getId());
        }

        fileData.setValue(bytes);

        repository.save(fileData);
    }

    private FileData findFileData(String fileId) {
        return repository.findByFileId(fileId).orElse(null);
    }
}

