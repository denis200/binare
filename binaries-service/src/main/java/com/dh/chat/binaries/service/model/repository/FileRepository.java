package com.dh.chat.binaries.service.model.repository;

import com.dh.chat.binaries.service.model.domain.FileImpl;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Santiago Mamani
 */
public interface FileRepository extends MongoRepository<FileImpl, String> {
}
