package com.dh.chat.binaries.service.model.repository;

import com.dh.chat.binaries.service.model.domain.FileData;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * @author Santiago Mamani
 */
public interface FileDataRepository extends MongoRepository<FileData, String> {

    Optional<FileData> findByFileId(String fileId);
}
