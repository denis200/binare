package com.dh.chat.binaries.service.model;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author Ivan Alban
 */
@Configuration
@EnableMongoRepositories(
        basePackages = "com.dh.chat.binaries.service.model.repository"
)
class ModelConfig {

}
