package com.dh.chat.binaries.service.service;

import com.dh.chat.binaries.service.exception.FileNotFoundException;
import com.dh.chat.binaries.service.model.domain.FileData;
import com.dh.chat.binaries.service.model.domain.FileImpl;
import com.dh.chat.binaries.service.model.repository.FileDataRepository;
import com.dh.chat.binaries.service.model.repository.FileRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class FileDataReadCmd implements BusinessLogicCommand {

    @Setter
    private String fileId;

    @Getter
    private byte[] bytes;

    @Getter
    private FileImpl file;

    @Autowired
    private FileDataRepository fileDataRepository;

    @Autowired
    private FileRepository fileRepository;

    @Override
    public void execute() {
        file = findFile(fileId);

        FileData fileData = fileDataRepository.findByFileId(fileId).orElse(null);

        if (null != fileData) {
            bytes = fileData.getValue();
        } else {
            bytes = new byte[0];
        }
    }

    private FileImpl findFile(String fileId) {
        return fileRepository.findById(fileId)
                .orElseThrow(() -> new FileNotFoundException("Unable to locate a file for binaryId '" + fileId + "'"));
    }
}
