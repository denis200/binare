package com.dh.chat.binaries.service.controller.api;

import com.dh.chat.binaries.service.service.FileDataReadCmd;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * @author Santiago Mamani
 */
@Api(
        tags = "file-controller",
        description = "Operations over files"
)
@RequestMapping(value = "/files")
@RestController
@RequestScope
public class DownloadFileController {

    @Autowired
    private FileDataReadCmd fileDataReadCmd;

    @ApiOperation(
            value = "Download a file"
    )
    @RequestMapping(
            value = "{fileId}/download",
            method = RequestMethod.GET
    )
    public ResponseEntity<InputStreamResource> download(@PathVariable("fileId") String fileId) {

        fileDataReadCmd.setFileId(fileId);
        fileDataReadCmd.execute();

        InputStream content = new ByteArrayInputStream(
                fileDataReadCmd.getBytes(),
                0,
                fileDataReadCmd.getBytes().length);

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "filename=" + fileDataReadCmd.getFile().getName())
                .contentType(MediaType.parseMediaType(fileDataReadCmd.getFile().getMimeType()))
                .body(new InputStreamResource(content));
    }

}
