package com.dh.chat.binaries.api.output;

/**
 * @author Santiago Mamani
 */
public interface File {

    String getId();

    String getMimeType();

    String getName();

    Long getSize();
}
